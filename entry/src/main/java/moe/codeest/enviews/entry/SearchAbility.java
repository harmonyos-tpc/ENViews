package moe.codeest.enviews.entry;

import moe.codeest.enviews.ENSearchView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

public class SearchAbility extends Ability {
    private ENSearchView searchView;
    private Button btnStart;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_search);
        searchView = (ENSearchView) findComponentById(ResourceTable.Id_view_search);
        btnStart = (Button) findComponentById(ResourceTable.Id_btn_search);
        btnStart.setClickedListener(component -> searchView.start());
    }
}
