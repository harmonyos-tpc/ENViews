package moe.codeest.enviews.entry;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class MainAbility extends Ability implements Component.ClickedListener {
    private Button btnRefresh;
    private Button btnPlay;
    private Button btnDownload;
    private Button btnScroll;
    private Button btnVolume;
    private Button btnSearch;
    private Button btnLoading;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);

        btnRefresh = (Button) findComponentById(ResourceTable.Id_btn_refresh);
        btnPlay = (Button) findComponentById(ResourceTable.Id_btn_play);
        btnDownload = (Button) findComponentById(ResourceTable.Id_btn_download);
        btnScroll = (Button) findComponentById(ResourceTable.Id_btn_scroll);
        btnSearch = (Button) findComponentById(ResourceTable.Id_btn_search);
        btnVolume = (Button) findComponentById(ResourceTable.Id_btn_volume);
        btnLoading = (Button) findComponentById(ResourceTable.Id_btn_loading);

        btnRefresh.setClickedListener(this);
        btnPlay.setClickedListener(this);
        btnDownload.setClickedListener(this);
        btnScroll.setClickedListener(this);
        btnSearch.setClickedListener(this);
        btnVolume.setClickedListener(this);
        btnLoading.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_refresh:
                gotoAbility(RefreshAbility.class);
                break;
            case ResourceTable.Id_btn_play:
                gotoAbility(PlayAbility.class);
                break;
            case ResourceTable.Id_btn_download:
                gotoAbility(DownloadAbility.class);
                break;
            case ResourceTable.Id_btn_scroll:
                gotoAbility(ScrollAbility.class);
                break;
            case ResourceTable.Id_btn_search:
                gotoAbility(SearchAbility.class);
                break;
            case ResourceTable.Id_btn_volume:
                gotoAbility(VolumeAbility.class);
                break;
            case ResourceTable.Id_btn_loading:
                gotoAbility(LoadingAbility.class);
                break;
        }
    }

    private void gotoAbility(Class clazz) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(this.getBundleName())
                .withAbilityName(clazz.getName())
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }


}
