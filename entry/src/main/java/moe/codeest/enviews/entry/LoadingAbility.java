package moe.codeest.enviews.entry;

import moe.codeest.enviews.ENLoadingView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

public class LoadingAbility extends Ability {
    private ENLoadingView loadingView;

    private Button btnShow;
    private Button btnHide;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_loading);
        loadingView = (ENLoadingView) findComponentById(ResourceTable.Id_view_loading);
        btnShow = (Button) findComponentById(ResourceTable.Id_btn_show);
        btnHide = (Button) findComponentById(ResourceTable.Id_btn_hide);
        btnShow.setClickedListener(component -> loadingView.show());
        btnHide.setClickedListener(component -> loadingView.hide());
    }
}
