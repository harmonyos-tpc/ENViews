package moe.codeest.enviews.entry;

import moe.codeest.enviews.ENPlayView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

public class PlayAbility extends Ability {
    private ENPlayView playView;

    private Button btnPause;
    private Button btnPlay;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_play);
        playView = (ENPlayView) findComponentById(ResourceTable.Id_view_play);
        btnPlay = (Button) findComponentById(ResourceTable.Id_btn_play);
        btnPause = (Button) findComponentById(ResourceTable.Id_btn_pause);
        btnPlay.setClickedListener(component -> playView.play());
        btnPause.setClickedListener(component -> playView.pause());
    }
}
