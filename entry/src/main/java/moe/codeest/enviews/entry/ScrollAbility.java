package moe.codeest.enviews.entry;

import moe.codeest.enviews.ENScrollView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

public class ScrollAbility extends Ability {
    private ENScrollView scrollView;
    private Button btnSwitch;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_scroll);
        scrollView = (ENScrollView) findComponentById(ResourceTable.Id_view_scroll);
        btnSwitch = (Button) findComponentById(ResourceTable.Id_btn_switch);
        btnSwitch.setClickedListener(component -> {
            if (scrollView.isSelected()) {
                scrollView.unSelect();
            } else {
                scrollView.select();
            }
        });
    }
}
