# ENViews

## Download
Gradle:

```groovy
repositories {
    mavenCentral()
}

dependencies{
    implementation 'io.openharmony.tpc.thirdlib:ENViews:1.0.1'
}
```


ENViews, A cool dynamic view library.All designed by [Nick Buturishvili](https://dribbble.com/nick_buturishvili
)  

ENViews, 一个华丽丽的动效控件库，所有控件原型取自[Nick Buturishvili](https://dribbble.com/nick_buturishvili
)的设计作品

# Preview

## RefreshView

<img src="art/demo_refresh.gif" width="40%"/>

## PlayView

<img src="art/demo_play.gif" width="40%"/>

## DownloadView

<img src="art/demo_download.gif" width="40%"/>

## ScrollView

<img src="art/demo_scroll.gif" width="40%"/>

## VolumeView

<img src="art/demo_volume.gif" width="40%"/>

## SearchView

<img src="art/demo_search.gif" width="40%"/>

## LoadingView

<img src="art/demo_loading.gif" width="40%"/>


# License

      Copyright (c) 2016 codeestX

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
