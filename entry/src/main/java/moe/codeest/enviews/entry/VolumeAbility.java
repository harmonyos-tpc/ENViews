package moe.codeest.enviews.entry;

import moe.codeest.enviews.ENVolumeView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Slider;

public class VolumeAbility extends Ability {
    private ENVolumeView volumeView;
    private Slider sliderVolume;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_volume);
        volumeView = (ENVolumeView) findComponentById(ResourceTable.Id_view_volume);
        sliderVolume = (Slider) findComponentById(ResourceTable.Id_sb_volume);
        sliderVolume.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                volumeView.updateVolumeValue(i);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
    }
}
