package moe.codeest.enviews.entry;

import moe.codeest.enviews.ENDownloadView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

public class DownloadAbility extends Ability {
    private ENDownloadView downloadView;

    private Button btnStart;
    private Button btnReset;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_download);

        downloadView = (ENDownloadView) findComponentById(ResourceTable.Id_view_download);
        btnStart = (Button) findComponentById(ResourceTable.Id_btn_start);
        btnReset = (Button) findComponentById(ResourceTable.Id_btn_reset);

        downloadView.setDownloadConfig(2000, 20, ENDownloadView.DownloadUnit.MB);

        btnStart.setClickedListener(component -> downloadView.start());

        btnReset.setClickedListener(component -> downloadView.reset());
    }
}
