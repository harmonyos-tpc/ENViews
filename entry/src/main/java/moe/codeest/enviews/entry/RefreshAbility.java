package moe.codeest.enviews.entry;

import moe.codeest.enviews.ENRefreshView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

public class RefreshAbility extends Ability {
    ENRefreshView refreshView;
    Button btnRefresh;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_refresh);
        refreshView = (ENRefreshView) findComponentById(ResourceTable.Id_view_refresh);
        btnRefresh = (Button) findComponentById(ResourceTable.Id_btn_refresh);
        btnRefresh.setClickedListener(component -> refreshView.startRefresh());
    }
}
